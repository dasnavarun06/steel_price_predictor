import pandas as pd
import random
from datetime import date, timedelta


start_date = date(2010, 1, 1)
end_date = date(2022, 1, 1)
date_range = [start_date + timedelta(days=i) for i in range((end_date - start_date).days)]

data = {
    "Date": date_range,
    "SteelPrice": [random.uniform(600, 700) for _ in range(len(date_range))],
}


commodity_prices = [random.uniform(100, 200) for _ in range(len(date_range))]
inventory_levels = [random.uniform(5000, 10000) for _ in range(len(date_range))]
gdp_values = [random.uniform(1, 10) for _ in range(len(date_range))]
inflation_rates = [random.uniform(0, 5) for _ in range(len(date_range))]

data["CommodityPrice"] = commodity_prices
data["InventoryLevel"] = inventory_levels
data["GDP"] = gdp_values
data["Inflation"] = inflation_rates


df = pd.DataFrame(data)


df.to_csv("historical_data.csv", index=False)
