import pandas as pd
import re
import requests
from bs4 import BeautifulSoup
from flask import Flask, request, jsonify
from prophet import Prophet
from flask_cors import CORS
from flask_cors import cross_origin


data = pd.read_csv("historical_data.csv")


data = data.rename (columns={"Date": "ds", "SteelPrice": "y"})
data["commodity_price"] = data["CommodityPrice"]
data["inventory"] = data["InventoryLevel"]
data["gdp"] = data["GDP"]
data["inflation"] = data["Inflation"]


model = Prophet(
    yearly_seasonality=20,  
    weekly_seasonality=True,
    daily_seasonality=True,
    changepoint_prior_scale=0.05,  
    holidays_prior_scale=10,  
    seasonality_prior_scale=15,  
)
model.add_seasonality(name="custom_commodity_price", period=365, fourier_order=10)
model.add_regressor("commodity_price")
model.add_regressor("inventory")
model.add_regressor("gdp")
model.add_regressor("inflation")


model.fit(data)


app = Flask(__name__)
CORS(app)


@app.route("/predict-user-input", methods=["POST"])
def predict_user_input():
    try:
        user_input = request.json

        
        user_input_df = pd.DataFrame(user_input)
        user_input_df = user_input_df.rename(columns={"Date": "ds", "CommodityPrice": "commodity_price", "InventoryLevel": "inventory", "GDP": "gdp", "Inflation": "inflation"})

        
        forecast = model.predict(user_input_df)

        
        
        def complex_prescription(row, user_gdp, user_inflation, user_commodity_price):
            threshold_price_low = 670
            threshold_price_medium = 673
            threshold_price_high = 674.5
            gdp_threshold_high = 5
            gdp_threshold_medium = 2
            gdp_threshold_low=1
            inflation_threshold_high = 5
            inflation_threshold_low=2

            prescription = ""
            reason = ""
            percentage_difference = 0.0

            if row[
                "yhat"] < threshold_price_low and user_gdp > gdp_threshold_high and user_commodity_price < threshold_price_low and inflation_threshold_low<user_inflation<inflation_threshold_high:
                percentage_difference = ((threshold_price_low - row["yhat"]) / threshold_price_low) * 100
                prescription = f"Buy Steel - The steel price is {percentage_difference:.2f}% below the threshold due to Low Price, High GDP growth, and Low Commodity Price."
            elif row[
                "yhat"] < threshold_price_medium and user_gdp > gdp_threshold_medium and user_commodity_price < threshold_price_medium and  inflation_threshold_low<user_inflation<inflation_threshold_high:
                percentage_difference = ((threshold_price_medium - row["yhat"]) / threshold_price_medium) * 100
                prescription = f"Buy Steel - The steel price is {percentage_difference:.2f}% below the threshold due to Medium Price, Medium GDP growth, and Low Commodity Price."
            elif row[
                "yhat"] > threshold_price_high and user_inflation > inflation_threshold_high and user_commodity_price > threshold_price_high and user_gdp < gdp_threshold_medium:
                percentage_difference = ((row["yhat"] - threshold_price_high) / threshold_price_high) * 100
                prescription = f"Sell Steel - The steel price is {percentage_difference:.2f}% above the threshold due to High Price, High Inflation, or High Commodity Price."
            elif user_gdp >= gdp_threshold_low and user_inflation >= inflation_threshold_low and threshold_price_low<row["yhat"] < threshold_price_high:
                prescription = "Hold - Steel as  Steel price, economic conditions, and commodity price are within acceptable ranges."
            else :
                prescription = "Monitor Closely - Consider holding, but keep an eye on economic conditions and steel price."



            return prescription

        

        
        user_gdp = user_input_df.iloc[0]["gdp"]
        user_inflation = user_input_df.iloc[0]["inflation"]
        user_commodity_price = user_input_df.iloc[0]["commodity_price"]

        
        forecast["Prescription"] = forecast.apply(lambda row: complex_prescription(row, user_gdp, user_inflation,user_commodity_price), axis=1)

        result = {
            "Predictions": forecast[["ds", "yhat"]].to_dict(orient="records"),
            "Prescription": forecast[["ds", "Prescription"]].to_dict(orient="records"),
        }

        return jsonify(result)
    except Exception as e:
        return jsonify({"error": str(e)})

@app.route("/steel-data", methods=['GET'])
def getSteelData():
    in_list = []
    table_dict = {}
    try:
        url = 'https://www.moneycontrol.com/commodity/ncdex/steel-price.html'

        response = requests.get(url)

        soup = BeautifulSoup(response.text, 'html.parser')

        date_container = soup.find('div', {'name': '120oct2023'})
        
        heading = date_container.find(class_ = 'FL brdr PR20 gr_13')

        if heading:
            heading_value = heading.find('span', class_='gr_30').get_text()
            changes = heading.find('strong').get_text()
        else:
            heading = data_container.find(class_ = 'FL brdr PR20 rd_13')
            heading_value = heading.find('span', class_='rd_30').get_text()
            changes = heading.find('strong').get_text()

        data_dict = {'NCDEX STEEL': heading_value, 'Change': changes}

        tables = soup.find('td', class_='PR10')

        for res in tables:
            in_list.append(res.text.strip())

        formatted_data = [re.sub(r'\s+', ' ', item).strip() for item in in_list if item.strip()]

        for item in formatted_data:
            matches = re.findall(r'([A-Za-z\s]+)\s*([\d,.()%-]+)', item)

            if matches:
                for key, value in matches:
                    table_dict[key.strip()] = value.strip()

        response_dict = data_dict.copy()
        response_dict.update(table_dict)

        return jsonify(response_dict)
    except Exception as e:
        return jsonify({"error": str(e)})

@app.route('/ping', methods=['GET'])
def ping():
    try:
        response = {"status": "application is up"}
        return jsonify(response)
    except Exception as e:
        return jsonify({"error": str(e)})

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8083, debug=False)
