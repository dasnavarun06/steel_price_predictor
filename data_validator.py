import pandas as pd


single_date_input = {
    "Date": ["2023-10-15"],
    "CommodityPrice": [650],
    "InventoryLevel": [5000],
    "GDP": [4.5],
    "Inflation": [2.0],
}


date_range_input = {
    "Date": ["2023-10-15", "2023-10-16", "2023-10-17"],
    "CommodityPrice": [650, 655, 660],
    "InventoryLevel": [5000, 4800, 4900],
    "GDP": [4.5, 5.2, 5.0],
    "Inflation": [2.0, 2.5, 2.2],
}


single_date_input_df = pd.DataFrame(single_date_input)
date_range_input_df = pd.DataFrame(date_range_input)


single_date_input_df['Date'] = pd.to_datetime(single_date_input_df['Date'])
date_range_input_df['Date'] = pd.to_datetime(date_range_input_df['Date'])


single_date_input_json = single_date_input_df.to_json(orient="records")
date_range_input_json = date_range_input_df.to_json(orient="records")

print("Single Date Input:")
print(single_date_input_json)

print("\nDate Range Input:")
print(date_range_input_json)
